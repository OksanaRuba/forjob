import React from 'react';
import ReactDOM  from 'react-dom'; //у інших компонентах можемо не зазначати головне, щоб був тут
import App from './components/app/app';


//обов'язково має бути getElementById, можемо назвати його як завгодно не лише root
ReactDOM.render(<App/>,document.getElementById('root'));