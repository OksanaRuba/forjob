import React from 'react';
import Header from '../header/header';
import ImageForm from '../image_form/image_form';
import Title from '../title/title';
import Text from '../text/text';
import Button from '../button/button';
import Footer from '../footer/footer';

// a combination of the other components in this folder
//components are written in capital letters

const App = () => {
    return (
        <div>
            <Header />
            <ImageForm />
            <Title />
            <Text />
            <Button />
            <Footer />
        </div>
    )
}

export default App;