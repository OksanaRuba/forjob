import React from 'react';

import './title.css';

const Title = () => {
    return ( // CSS-класи, як правило, краще для продуктивності, ніж вбудовані стилі.
        <div className='Title'>
            <p>
                Rappresent your life with a simple photo
            </p>
        </div>
    )
}

export default Title;
