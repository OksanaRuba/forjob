import React from 'react';
import HeaderList from './header_list/header_list';

import '../header_ul/header_ul.css'; // робимо відступи між компонентами
// в ul import li

const HeaderUl = () => {
    return (
        <div>
            <ul>
                <HeaderList />
            </ul>
        </div>
    )
}
export default HeaderUl;