import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import Calculator from './components/calculator';


ReactDOM.render(//This will make our repository instance available to all components that reside in the Provider component.
  <Provider store={store}> 
    <Calculator />
  </Provider>, document.getElementById('root')
);




