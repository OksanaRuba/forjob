import { createStore } from 'redux';
import { calcReducer } from '../redux/rootReducer';

export default createStore(
  calcReducer
);

//Store keeps the state of the application. The only way to change store is by dispatch action.