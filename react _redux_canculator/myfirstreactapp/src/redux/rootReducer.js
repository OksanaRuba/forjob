import { ADD_OPERATION, CLEAR, EQUAL } from '../redux/types';

const defaultValue = {
    value: 0
} 
//"Actions describes the fact that something happened, but does not indicate how the state of the application should change in response, this is a job for Reducer "(off documentation)

 export const calcReducer = (state = defaultValue, action) => {
    switch(action.type){
        case 'ADD_OPERATION':
            return{
                ...state,
                value: state.value == 0 ? action.payload : state.value + action.payload
            }
        case 'CLEAR':
            return{
                ...state,
                value: 0
            }
        case 'EQUAL':
            console.log('action', action);
            return{
                ...state,
                value: eval(action.payload) //The eval() function is dangerous in terms of security.
            }
        default:
            return state;
    }
};
