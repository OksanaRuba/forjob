import { ADD_OPERATION, CLEAR, EQUAL } from '../redux/types';

// An action is a JavaScript object that succinctly describes the essence of change. This is necessarily the presence of the type property, the value of which is usually a string.
export function add_operation(payload){
    return { 
        type: ADD_OPERATION,
        payload 
    }
}

export function clear(){
    return {
        type: CLEAR
    }
}

export function equal(payload){
    return {
        type: EQUAL,
        payload 
    }
}
