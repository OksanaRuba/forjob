import React from 'react';
import { connect } from 'react-redux'; //connect the React component to the Redux store

import './calculator.css';


function Calculator(props) {
  const { value, add_operation, clear, equal } = props;
  return (
    <div className="box">
      <div className="display">
        <input className='text' type="text" readOnly size="18" value={value} />
      </div>
      <div className="keys">
        <p>
          <input type="button" className="button gray" value="mrc" />
          <input type="button" className="button gray" value="m-" />
          <input type="button" className="button gray" value="m+" />
          <input type="button" className="button pink" value="/" onClick={e => add_operation(e.target.value)} />
        </p>
        <p>
          <input type="button" className="button black" value="7" onClick={e => add_operation(e.target.value)} />
          <input type="button" className="button black" value="8" onClick={e => add_operation(e.target.value)} />
          <input type="button" className="button black" value="9" onClick={e => add_operation(e.target.value)} />
          <input type="button" className="button pink" value="*" onClick={e => add_operation(e.target.value)} />
        </p>
        <p>
          <input type="button" className="button black" value="4" onClick={e => add_operation(e.target.value)} />
          <input type="button" className="button black" value="5" onClick={e => add_operation(e.target.value)} />
          <input type="button" className="button black" value="6" onClick={e => add_operation(e.target.value)} />
          <input type="button" className="button pink" value="-" onClick={e => add_operation(e.target.value)} />
        </p>
        <p>
          <input type="button" className="button black" value="1" onClick={e => add_operation(e.target.value)} />
          <input type="button" className="button black" value="2" onClick={e => add_operation(e.target.value)} />
          <input type="button" className="button black" value="3" onClick={e => add_operation(e.target.value)} />
          <input type="button" className="button pink" value="+" onClick={e => add_operation(e.target.value)} />
        </p>
        <p>
          <input type="button" className="button black" value="0" onClick={e => add_operation(e.target.value)} />
          <input type="button" className="button black" value="." onClick={e => add_operation(e.target.value)} />
          <input type="button" className="button black" value="C" onClick={clear} />
          <input type="button" className="button orange" value="=" onClick={() => equal(value)} />
        </p>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  //take the entire "state" of the application and attach it to the value variable so that it is available from the Calculator.js component like this.props.value
  return {
    value: state.value
  }
}
const mapDispatchToProps = (dispatch) => {
  // to be called each time the state of the repository changes. The first parameter is the current state of the Redux repository.
  return {
    add_operation: (payload) => {
      dispatch({
        type: 'ADD_OPERATION',
        payload
      })
    },
    clear: () => {
      dispatch({
        type: 'CLEAR'
      })
    },
    equal: (payload) => {
      dispatch({
        type: 'EQUAL',
        payload
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);



