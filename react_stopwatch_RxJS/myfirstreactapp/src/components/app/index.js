import React from "react";
import { useEffect, useState } from "react"; //using a functional approach we use hooks which we take from the library react
import { interval, Subject } from "rxjs"; 
import { takeUntil } from "rxjs/operators";

import "./app.css";

export default function App() {
    const [sec, setSec] = useState(0);
    const [timer, setTimer] = useState(false);

    useEffect(() => {
        const stream$ = new Subject();// create stream, sign and then work out the terms and put an interval in it which will be started every second and add a second
        interval(1000)
            .pipe(takeUntil(stream$))//similar to then in the promise
            .subscribe(() => { //we sign and then work out the condition
                if (timer === "run") {
                    setSec(val => val + 1000);//iteration
                }
            });
        return () => {
            stream$.next();//method to get the next event
            stream$.complete();//method for performing actions on the closure of the event source
        };
    }, [timer]);

    const start = () => {
        setTimer("run");
    };

    const stop = () => {
        setTimer("stop");
        setSec(0);
    };

    const reset = () => {
        setSec(0);
    };

    const wait = () => {
        setTimer("wait");
    };

    return (
        <div className="block"> 
            <span className="time"> {new Date(sec).toISOString().slice(11, 19)}</span> 
            <button className="button" onClick={start}>Start</button>
            <button className="button" onClick={stop}>Stop</button>
            <button className="button" onClick={reset}>Reset</button>
            <button className="button" onClick={wait}>Wait</button>
        </div>
    );
};

/*using only react

import React, { useState, useEffect } from 'react';

function App() {
    const [time, setTime] = useState(0);
    const [timerOn, setTimeOn] = useState(false);

    useEffect(() => {
        let interval = null;

        if(timerOn) {
            interval = setInterval(() => {
                setTime(prevTime => prevTime + 10)
            }, 10)
        } else {
            clearInterval(interval)
        }

        return () => clearInterval(interval)

    }, [timerOn])


    return (
        <div className='App'>
            <div>{("0" + ((time / 10) % 100)).slice(-2)}</div>
            <div>
                <button onClick={() => setTimeOn(true)}>Start</button>
                <button onClick={() => setTimeOn(false)}>Stop</button>
                <button onClick={() => setTimeOn(true)}>Wait</button>
                <button onClick={() => setTime(0)}>Reset</button>
            </div>
        </div>
    );
}

export default App; */