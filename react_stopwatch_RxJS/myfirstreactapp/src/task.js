/*
Реализовать секундомер, который подсчитывает время в формате «HH: MM: SS»

Секундомер должен иметь следующие кнопки:

* «Start / Stop» - запуск / остановка отсчета времени, останавливает и обнуляет значение секундомера.

* «Wait» - работает на двойной клик (время между нажатиями не более 300 мс!) секундомер должен прекратить отсчет времени; если после него нажать старт, то возобновляется отсчет.

* «Reset» - сброс секундомера на 0.  Обнуляет секундомер и снова начинает отсчет.

Требования:

 - используйте Observables в коде

 - RxJS подход

 - функциональный подход

 - нам важнее всего увидеть Ваше умение писать код

- 300 млс – это не DoubleClick («Wait» не зрозуміла як робити).


// 
Subject
— это объект, который одновременно умеет как принимать значения, так и выдавать. Главное отличие Subject’a от обычного Observable — это то, что мы можем генерировать новое событие вручную, с помощью функции .next(), в то время как в Observable новые значения должны поступать из callback-функции. Также мы можем вручную завершить поток функцией .complete(); или вызвать функцию завершения потока с ошибкой .error();

Пример: В примере ниже мы создаем Subject и кладем в него значения от 0 до 4 через каждую секунду. Создаем подписку A сразу после объявления Subject, а подписку B через 2,5 секунды.

const subject = new Subject();
const source1 = timer(1000, 1000).pipe(
    take(5),
    tap( (x) => subject.next(x) )
).subscribe();
const sub1 = subject.subscribe(x => console.log('A: ', x));
setTimeout(() => {
    const sub2 = subject.subscribe(x => console.log('B: ', x));
}, 2500);

takeUntil 
subscribes and begins mirroring the source Observable. It also monitors a second Observable,
notifier that you provide. If the notifier emits a value, the output Observable stops mirroring the source Observable and completes. 
If the notifier doesn't emit any value and completes then takeUntil will pass all values.
*/